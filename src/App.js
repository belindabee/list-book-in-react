import React, { Component } from "react"
import BookList from "./component/BookList"
import "./App.css"

class App extends Component {
  state = {
    books: [
      {
        id: 1,
        image: require("./assets/image/image1.jpg"),
        name: "Rich Dad Poor Dad",
        description:
          "Rich Dad Poor Dad is a 1997 book written by Robert Kiyosaki and Sharon Lechter. It advocates the importance of financial literacy (financial education), financial independence and building wealth through investing in assets, real estate investing, starting and owning businesses, as well as increasing one's financial intelligence (financial IQ) to improve one's business and financial aptitude. Rich Dad Poor Dad is written in the style of a set of parables, ostensibly based on Kiyosaki's life.",
        author: "Robert Kiyosaki"
      },
      {
        id: 2,
        image: require("./assets/image/image2.jpg"),
        name: "Think and Grow Rich",
        description:
          "Think and Grow Rich was written by Napoleon Hill in 1937 and promoted as a personal development and self-improvement book. He claimed to be inspired by a suggestion from business magnate and later-philanthropist Andrew Carnegie",
        author: "Napoleon Hill"
      },
      {
        id: 3,
        image: require("./assets/image/image3.jpg"),
        name: "The Magic of Think Big",
        description:
          "The Magic of Thinking Big, first published in 1959, is a self-help book by David J. Schwartz. An abridged version was published in 1987. Forbes called it one of the greatest self-help books",
        author: "Ben Carson, M.D., Cecil Murphey"
      },
      {
        id: 4,
        image: require("./assets/image/image4.jpg"),
        name: "Reason To Stay Alive",
        description:
          "The book is about making the most of one's time on earth written based on the writer's experience from his own life. At the age of 24, the writer himself suffered from severe depression. In the book, he unfolded how he recovered and learned to live with depression.",
        author: "Matt Haigh"
      }
    ]
  }

  render() {
    return (
      <div>
        <BookList books={this.state.books} />
      </div>
    )
  }
}
export default App

import React, { Component } from "react"

class BookList extends Component {
  render() {
    const book = this.props.books.map(book => {
      const { image, name, description, author } = book
      return (
        <div style={lebar}>
          <img
            src={image}
            alt={name}
            style={{ width: "10rem", height: "15rem" }}
          />
          <h2>{name}</h2>
          <span>by: {author}</span>
          <p>{description}</p>
        </div>
      )
    })

    return (
      <div>
        {/* <Link to="/">
          <button>Home</button>
        </Link> */}
        <div style={flex}> {book} </div>
      </div>
    )
  }
}

const lebar = {
  padding: "20px 10px",
  width: "30%"
}

const flex = {
  display: "flex"
}

export default BookList
